'use strict'

const gulp = require('gulp');
const sass = require('gulp-sass');
const browsersync = require('browser-sync');
const eslint = require('gulp-eslint');
const concat = require('gulp-concat');
const imagemin = require('gulp-imagemin');
const cssmin = require('gulp-minify-css');
const uglify = require('gulp-uglify');
const sourcemaps = require('gulp-sourcemaps');
const reload = browsersync.reload;
const del = require('del');
const stylish = require('eslint-stylish');
const watch = require('gulp-watch');
const merge = require('merge-stream');

const paths = {
  angular: [
    './bower_components/angular/angular.js',
    './bower_components/angular-ui-router/release/angular-ui-router.js'
  ],
  vendors: [
    './bower_components/leaflet/dist/leaflet.js',
  ],
  vendors_style: [
    './bower_components/leaflet/dist/leaflet.css'
  ]
};

const config = {
  server: {
    baseDir: "./build"
  },
  tunnel: true,
  host: 'localhost',
  port: 8080,
  logPrefix: 'AngularProject'
};

gulp.task('clean', function(){
  return del('./build/*', {
    force: true
  });
});

gulp.task('lint', function() {
  return gulp.src('./src/app/**/*.js')
    .pipe(eslint('.eslintrc'))
    .pipe(eslint.format());
});

gulp.task('js-app', function() {
  return gulp.src(['./src/**/module.js', './src/**/*.js'])
    .pipe(concat('app.js'))
    .pipe(gulp.dest('./build'))
    .pipe(reload({stream: true}));
});

gulp.task('js', () => {
  
  var angular = gulp.src(paths.angular)
    .pipe(concat('angular.js'))
    .pipe(gulp.dest('./build'));
  
  var leaflet = gulp.src(paths.vendors)
    .pipe(concat('vendors.js'))
    .pipe(gulp.dest('./build'));
  
  return merge(angular, leaflet);
});

gulp.task('html', function() {
  return gulp.src(['./src/app/**/*.html'])
    .pipe(gulp.dest('./build'))
    .pipe(reload({stream: true}));
});

gulp.task('copy', function() {
  var index = gulp.src('./src/app/index.html')
    .pipe(gulp.dest('./build'));
  
  var images = gulp.src('./src/assets/images/*.*')
    .pipe(gulp.dest('./build/img'));
  
  return merge(index, images);
});

gulp.task('scss', function() {
  var scss = gulp.src('./src/assets/scss/main.scss')
    .pipe(sass())
    .pipe(concat('style.css'))
    .pipe(gulp.dest('./build'))
    .pipe(reload({stream: true}));
  
  var vendors = gulp.src(paths.vendors_style)
    .pipe(concat('vendors.css'))
    .pipe(gulp.dest('./build'));
  
  return merge (scss, vendors);
});

gulp.task('fonts', function() {
  return gulp.src(['./src/assets/fonts/*.ttf'])
    .pipe(gulp.dest('./build/fonts'));
})

gulp.task('webserver', function() {
  browsersync(config);
});

gulp.task('watch', function(callback) {
  gulp.watch('./src/**/*.js', gulp.parallel('lint', 'js-app'));
  gulp.watch('./src/**/*.html', gulp.series('html'));
  gulp.watch('./src/**/*.scss', gulp.series('scss'));
  callback();
});

gulp.task('build', gulp.series('clean', gulp.parallel('lint', 'copy', 'js', 'js-app', 'html', 'scss', 'fonts')));

gulp.task('default', gulp.series('build', gulp.parallel('watch', 'webserver')));
