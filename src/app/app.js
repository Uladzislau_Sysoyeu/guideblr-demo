/* global document, angular, L */

(function() {
  'use strict';

  angular.element(document).ready(onDocumentReady);

  angular
    .module('map', [
      'ui.router',
      'map.layout',
      'map.index',
      'map.places',
      'map.services',
      'place.create'
    ])
    .config(config)
    .run(run);

  function onDocumentReady() {
    angular.bootstrap(document, ['map']);
  }

  /* @ngInject */
  function run() {
    L.Icon.Default.imagePath = './img/';
  }

  /* @ngInject */
  function config($urlRouterProvider) {
    $urlRouterProvider.otherwise('/');
  }

})();
