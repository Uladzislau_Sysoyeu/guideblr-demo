/*global angular */

(function() {
  'use strict'
  
  angular
    .module('map')
    .directive('backgroundImage', BackgroundImage);
  
  function BackgroundImage() {
    return {
      restrict: 'A',
      scope: {
        backgroundImage: '='
      },
      link: link
    };
    
    function link($scope, $el) {
      $el.css('background-image', 'url(' + $scope.backgroundImage + ')');
    }
  }
})();
