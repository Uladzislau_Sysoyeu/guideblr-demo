/*global angular, L */

(function() {
  'use strict'
  
  angular
    .module('map')
    .directive('leafletMap', Map);
  
  function Map() {
    return {
      restrict: 'EA',
      replace: true,
      template: '<div class="map" id="map"></div>',
      scope: {
        markers: '=',
        onMarkerClick: '&',
        config: '='
      },
      link: link
    };
    
    function link($scope, $el) {
      var map = L.map($el[0]).setView([53.918273, 27.584792], 7);
      L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
      }).addTo(map);
      
      $scope.markers = $scope.markers || [];
      $scope.$watch(function() {
        return $scope.markers.length;
      }, onMarkersChange);
      
      function onMarkersChange() {
        renderMarkers();
      }
      
      function renderMarkers() {
        $scope.markers.forEach(renderMarker);
      }
      
      function renderMarker(place) {
        var myIcon = L.icon({
          iconUrl: place.picture,
          iconSize: [50, 50],
          className: 'place__map-marker'
        });

       L.marker([place.lat, place.lng], {icon: myIcon})
         .addTo(map)
         .on('click', function(marker) {
          console.log(marker);
          $scope.onMarkerClick({ id: place._id });
        });
         //.bindPopup(place.description);
      }
      
    }
  }
})();
