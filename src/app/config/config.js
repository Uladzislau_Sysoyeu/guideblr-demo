/* global angular */

(function() {
  'use strict'
  angular
    .module('map')
    .constant('APP_CONFIG', {
      REST_ROOT: 'http://localhost:3000/'
    });
  
})();
