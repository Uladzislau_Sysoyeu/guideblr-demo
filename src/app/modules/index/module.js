/* global angular */

(function() {
  'use strict';

  angular
    .module('map.index', [])
    .config(config);

  /* @ngInject */
  function config($stateProvider) {
    $stateProvider
      .state('root.index', {
        url: '/',
        views: {
          'content': {
            templateUrl: 'modules/index/index/template.html',
            controller: 'IndexCtrl'
          }
        }
      });      
  }

})();
