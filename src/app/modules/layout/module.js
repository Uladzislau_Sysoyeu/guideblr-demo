/* global angular */

(function() {
  'use strict';

  angular
    .module('map.layout', [])
    .config(config);

  /* @ngInject */
  function config($stateProvider) {
    $stateProvider
      .state('root', {
        url: '',
        abstract: true,
        views: {
          '@': {
            templateUrl: 'modules/layout/layout/template.html'
          },
          'header@root': {
            templateUrl: 'modules/layout/header/template.html'
          }   
        }
      });
  }

})();
