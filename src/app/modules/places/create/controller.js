/* global angular */

(function() {
  'use strict';

  angular
    .module('place.create', [])
    .controller('CreatePlaceCtrl', CreatePlaceCtrl);

  /* @ngInject */
  function CreatePlaceCtrl($scope, $state, PlaceAPI) {
    angular.extend($scope, {
      create: create
    });
    
    function create(form) {
      if (form.$invalid) {
        return;
      }
      
      PlaceAPI
        .create($scope.sight)
        .then(function (response) {
          $state.go('root.places');
          console.log(response.data);  
        });
    }
  }

})();
