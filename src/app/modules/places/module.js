/* global angular */

(function() {
  'use strict';

  angular
    .module('map.places', [])
    .config(config);

  /* @ngInject */
  function config($stateProvider) {
    $stateProvider
      .state('root.places', {
        url: '/places',
        views: {
          'content': {
            templateUrl: 'modules/places/places/template.html',
            controller: 'PlacesCtrl'
          }
        }
      })
      .state('root.create-place', {
        url: '/places/create',
        views: {
          'content': {
            templateUrl: 'modules/places/create/template.html',
            controller: 'CreatePlaceCtrl'
          }
        }
      })
      .state('root.place', {
        url: '/places/:id',
        views: {
          'content': {
            templateUrl: 'modules/places/place/template.html',
            controller: 'PlaceCtrl'
          }
        }
      });

  }

})();