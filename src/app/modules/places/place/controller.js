/* global angular */

(function() {
  'use strict';

  angular
    .module('map')
    .controller('PlaceCtrl', PlaceCtrl);

  /* @ngInject */
  function PlaceCtrl($scope, $stateParams, PlaceAPI, $state) {
    angular.extend($scope, {
      update: update,
      remove: remove
    });
    
    PlaceAPI
      .find($stateParams.id)
      .then(function(response) {
        $scope.place = response.data.sight;
      });
    
    function update(id, form) {
      var data = angular.copy($scope.place);
      
      PlaceAPI
        .update($stateParams.id, data)
        .then(function (response) {
          $state.go('root.places');
        });
    };
    
    function remove(id) {
      PlaceAPI
        .remove($stateParams.id)
        .then(function(response) {
          $state.go('root.places');
        });
    };
  }

})();
