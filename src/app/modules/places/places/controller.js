/* global angular */

(function() {
  'use strict';

  angular
    .module('map.places')
    .controller('PlacesCtrl', PlacesCtrl);

  /* @ngInject */
  function PlacesCtrl($scope, $state, PlaceAPI) {
    angular.extend($scope, {
      config: {
        
      }
    });
    
    angular.extend($scope, {
      onMarkerClick: onMarkerClick
    });
    
    function onMarkerClick(id) {
      $state.go('root.place', { id: id });
    }
    
    PlaceAPI
      .all()
      .then(function(response) {
        $scope.places = response.data.sights;
      });
  }

})();
