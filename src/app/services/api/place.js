/* global angular */

(function() {
  'use strict'
  
  angular
    .module('map.services')
    .factory('PlaceAPI', PlaceAPI);
  
  function PlaceAPI($http, APP_CONFIG){
    var service = {
      all: all,
      find: find,
      create: create,
      update: update,
      remove: remove
    };
    
    return service;
    
    function all() {
      return $http.get(APP_CONFIG.REST_ROOT + 'places');     
    }

    function find(id) {
      return $http.get(APP_CONFIG.REST_ROOT + 'places/' + id); 
    }

    function create(sight) {
      return $http.post(APP_CONFIG.REST_ROOT + 'places', { sight: sight }); 
    }
    
    function update(id, updates) {
      return $http.put(APP_CONFIG.REST_ROOT + 'places/' + id, { updates: updates });
    }
    
    function remove(id) {
      return $http.delete(APP_CONFIG.REST_ROOT + 'places/' + id);
    }
    
  }

})();
